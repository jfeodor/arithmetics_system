import numpy
from numerous.client import NumerousClient


def evaluate(component) -> float:
    """Recursively evaluates the given component, and returns the result."""
    if component.type == 'FloatValue':
        return float(component.value)
    elif component.type == 'Plus':
        x = evaluate(component.input('x').resolve())
        y = evaluate(component.input('y').resolve())
        return numpy.sum(numpy.asarray([x, y]))
    else:
        raise RuntimeError(f'Cannot evaluate component {component}')


def run(client: NumerousClient, system):
    """Runs the arithmetic evaluation, by evaluating the component that is connected to the output."""
    result = evaluate(system.implementation.output('Result').resolve())
    client.set_scenario_results(['result'], [result], ['integer'])
